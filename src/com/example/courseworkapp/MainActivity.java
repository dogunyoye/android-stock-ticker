package com.example.courseworkapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	private TextView outputBox;
	private EditText inputBox;
	private CSVReader csv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		outputBox = (TextView) findViewById(R.id.textView3);
		inputBox = (EditText) findViewById(R.id.editText1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void searchAction(View view) throws IOException, InterruptedException{

		String x = inputBox.getText().toString();
		getData("http://finance.yahoo.com/d/quotes.csv?s=" + x + "&f=dj1l1pxn");
		outputBox.setText(Html.fromHtml(arrangeData(csv)));
		inputBox.setText("");
	}

	public String arrangeData(CSVReader csvData) throws IOException
	{
		double firstNumber,secondNumber,priceChange = 0,priceChangePercentage = 0;
		String data = "";
		String rowOfStars = "*********************";
		currencySymbol symbol = new currencySymbol(); // instantiates "symbol" as an instance of the currencySymbol class to later obtain the stock's currency symbol.
		String[] temp = csvData.readNext(); 

		if(temp[1].contains("N/A")) // catches any invalid stock queries if the market cap is "N/A". This was used instead of the error flag to prevent missing invalid stock symbols.
		{
			data = "ERROR: Symbol is not valid!" + "<p>" + "</p>";
			inputBox.setText("");
			return data;
		}

		else
		{	

			firstNumber = Double.parseDouble(temp[2]);
			secondNumber = Double.parseDouble(temp[3]);
			priceChange = firstNumber - secondNumber;
			priceChangePercentage = ((firstNumber - secondNumber)/secondNumber)*100;

			DecimalFormat twoDP = new DecimalFormat("0.00");  // rounds the price change to two decimal places.
			String pChangePercentage = twoDP.format(priceChangePercentage);
			String pChange = twoDP.format(priceChange);

			if(priceChangePercentage > 0) // if the price change percentage is greater than 0, then the price's colour is set to green
			{
				pChangePercentage = "<font color=\"green\">(+" + pChangePercentage + "%)</font>";
				pChange = "<font color=\"green\">+" + pChange + "</font>";
			}
			else if(priceChangePercentage < 0) // if the price change percentage is less than 0, then the price's colour is set to red
			{
				pChangePercentage = "<font color=\"red\">(" + pChangePercentage + "%)</font>";
				pChange = "<font color=\"red\">" + pChange + "</font>";
			}
			else // if neither of the above conditions are satisfied, the colour stays black
			{
				pChangePercentage = "(" + pChangePercentage + "%)";
				pChange = "" + pChange;
			}

			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(temp[5].replaceAll("\"", ""));
			stringBuilder.append("<br>");
			stringBuilder.append(rowOfStars);
			stringBuilder.append("</br>");
			stringBuilder.append("<br>");
			stringBuilder.append("Price: ");
			stringBuilder.append(twoDP.format(Double.parseDouble(temp[3])));
			stringBuilder.append(symbol.getCurrencySymbol(temp[4]));
			stringBuilder.append("</br>");
			stringBuilder.append("<br>");
			stringBuilder.append("Change: ");
			stringBuilder.append(pChange);
			stringBuilder.append(pChangePercentage);
			stringBuilder.append("</br>");
			stringBuilder.append("<br>");
			stringBuilder.append("Divident: ");
			stringBuilder.append(temp[0]);
			stringBuilder.append("</br>");
			stringBuilder.append("<br>");
			stringBuilder.append("Market Cap: ");
			stringBuilder.append(temp[1]);
			stringBuilder.append("</br>");
			stringBuilder.append("<br>");
			stringBuilder.append("Stock Exchange: ");
			stringBuilder.append(temp[4].replaceAll("\"", ""));
			stringBuilder.append("</br>");
			stringBuilder.append("<p>");
			stringBuilder.append("</p>");
			data = (stringBuilder.toString());
		}

		return data;
	}

	public void getData(final String url) throws InterruptedException{

		Thread thread = new Thread(new Runnable(){
			@Override
			public void run() {
				try{

					HttpClient httpclient = new DefaultHttpClient();  
					HttpGet request = new HttpGet(url);  
					// Execute HTTP GET Request  
					HttpResponse response = httpclient.execute(request);
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					csv = new CSVReader(br);

				} catch (ClientProtocolException e) {  
					// TODO Auto-generated catch block 
				} catch (IOException e) {  
					// TODO Auto-generated catch block 
				}  
			}

		});

		thread.start();
		thread.join();
	}

}

