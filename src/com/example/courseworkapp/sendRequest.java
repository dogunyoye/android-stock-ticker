package com.example.courseworkapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class sendRequest {
	
	private String urlString;
	private BufferedReader br;
	
	
	public sendRequest(final String url){
		
		this.urlString = url;
		
		Thread thread = new Thread(new Runnable(){
		    @Override
		    public void run() {
		    	try{
		    		
		    	   HttpClient httpclient = new DefaultHttpClient();  
	               HttpGet request = new HttpGet(urlString);  
	          
	               // Execute HTTP Post Request  
	               HttpResponse response = httpclient.execute(request);
	               InputStream is = response.getEntity().getContent();
	               Reader reader = new InputStreamReader(is);
	               br = new BufferedReader(reader);
	               CSVReader csv = new CSVReader(br);
	               
	               String [] nextLine;
	       		while ((nextLine = csv.readNext()) != null) {
	       			Log.i("TEST","Name: [" + nextLine[0] + "]\nAddress: [" + nextLine[1] + "]\nEmail: [" + nextLine[2] + "]");
	       		}

	            } catch (ClientProtocolException e) {  
	                // TODO Auto-generated catch block 
	                
	           } catch (IOException e) {  
	                // TODO Auto-generated catch block 
	                    
	           }  
	            }
	    
		});

		thread.start();
	}
	

}
